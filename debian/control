Source: blogofile
Section: web
Priority: optional
Maintainer: Jakob Haufe <sur5r@sur5r.net>
Build-Depends: debhelper (>= 8), python-setuptools (>= 0.6b3), python-all
Standards-Version: 3.9.4
Homepage: http://www.blogofile.com/
Vcs-Git: https://salsa.debian.org/debian/blogofile.git
Vcs-Browser: https://salsa.debian.org/debian/blogofile

Package: blogofile
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-pkg-resources
Suggests: git | subversion | bzr | mercurial | darcs
Description: static website compiler and blog engine
 Blogofile is a static website compiler, primarily (though not
 exclusively) designed to be a simple blogging engine. It requires no
 database and no special hosting environment. You customize a set of
 templates with Mako, create posts in a markup language of your choice
 (Markdown, Textile, reStructuredText or plain old HTML) and Blogofile
 renders your entire website as static HTML and Atom/RSS feeds which
 you can then upload to any old web server you like.

Package: blogofile-converters
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-mysqldb, python-sqlalchemy
Recommends: blogofile (>= ${source:Version})
Description: blog converter collection for Blogofile
 This is a collection of scripts which helps to migrate existing Blogs
 into the Blogofile /_posts directory format.
 .
 The included converters are:
 .
  * wordpress2blogofile: converts a Wordpress Blog
